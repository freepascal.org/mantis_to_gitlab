program mantistogitlab;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils, CustApp, fpjson, jsonparser, gitlabclient, Inifiles,
  mantisclient, sqldbini, opensslsockets, mgconvert, typinfo, strutils;

type
  TRunMode = (rmListProjects,rmListMilestones,rmListMantisProjects,rmInfo,rmConvert,rmDelete);


  { TMantis2GitlabApplication }

  TMantis2GitlabApplication = class(TCustomApplication)
  private
    FRunMode: TRunMode;
    FGitlabConfig : TGitlabConfig;
    FMantisClient : TMantisClient;
    FGitlabClient : TGitlabClient;
    FConversionConfig : TConversionConfig;
    FTimeStamp : Boolean;
    FQuiet : Boolean;
    LogFileName : String;
    Flog : TFileStream;
    procedure CloseLog;
    function GetProjectResourceURL(aResource: string): String;
    procedure OpenLog;
  protected
    // General
    procedure LoadConfig(aConfigFile : String);
    function ParseOptions: String;
    // Output
    procedure DoLog(Sender: TObject; const aMessage: string); reintroduce;
    procedure ListObjects(aList: TJSONArray; aName: string; aMembers: array of string);
    procedure ShowObject(aIndex: Integer; aObj: TJSONObject;
      aMembers: array of string);
    procedure DoRun; override;
    procedure DoListProjects;
    Procedure DoListMileStones;
    procedure DoShowInfo;
    procedure DoConversion;
    procedure DoDelete;
    procedure DoListMantisProjects;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure Usage(const aMsg : String); virtual;
    Property RunMode : TRunMode Read FRunMode;
  end;

{ TGitLabClient }

{ TGitLabRequest }


{ TMantis2GitlabApplication }

function TMantis2GitlabApplication.GetProjectResourceURL(aResource: string): String;

begin
  Result:=Format('projects/%d/%s/',[FConversionConfig.GitlabProjectID,aResource]);
end;

procedure TMantis2GitlabApplication.LoadConfig(aConfigFile: String);

Var
  aIni : TCustomIniFile;

  Function CheckPart(aParts : TParts; aName : String; aPart : TPart) : TParts;
  begin
    Result:=aParts;
    if aIni.ReadBool('Conversion',aName,False) then
      Include(Result,aPart);
  end;

begin
  aIni:=TMemIniFile.Create(aConfigFile);
  try
    With aIni,FGitLabConfig do
      begin
      BaseURL:=ReadString('Gitlab','BaseURL',BaseURL);
      APIkey:=ReadString('Gitlab','Apikey',APIKey);
      end;
    FMantisClient.Conn.LoadFromIni(aIni,'Mantis',[sioSkipMaskPassword]);
    With aIni,FConversionConfig do
      begin
      GitlabProjectID:=ReadInt64('Conversion','GitlabProjectID',GitlabProjectID);
      MantisProjectID:=ReadInt64('Conversion','MantisProjectID',MantisProjectID);
      StartID:=ReadInt64('Conversion','BugStartID',StartID);
      Count:=ReadInt64('Conversion','BugCount',Count);
      TargetVersionLabelField:=ReadString('Conversion','VersionLabel',TargetVersionLabelField);
      UserMapFile:=ReadString('Conversion','UserMapFile','');
      ProjectMapFile:=ReadString('Conversion','ProjectMapFile','');
      RevisionMapFile:=ReadString('Conversion','RevisionMapFile','');
      ExtraFieldLabel:=ReadString('Conversion','ExtraFieldLabel','');
      ExtraFieldName:=ReadString('Conversion','ExtraFieldName','');
      BugFileDir:=ReadString('Conversion','BugFileDir','');
      UserMapCategory:=ReadString('Conversion','UserMapCategory','');
      UserMapProjectID:=ReadInt64('Conversion','UserMapProjectID',0);
      GitlabUserIDMapFile:=ReadString('Conversion','GitlabIDMapFile','');
      FixedInRevisionField:=ReadString('Conversion','FixedInRevisionField','');
      ExcludeCategories:=ReadString('Conversion','ExcludeCategories','');
      OnlyCategories:=ReadString('Conversion','OnlyCategories','');
      ExtraFieldUnscoped:=ReadBool('Conversion','ExtraFieldUnscoped',False);
      UpdateBugs:=ReadString('Conversion','UpdateBugs','');
      Parts:=[];
      Parts:=CheckPart(Parts,'VersionLabels',pVersionLabels);
      Parts:=CheckPart(Parts,'VersionMileStones',pVersionMileStones);
      Parts:=CheckPart(Parts,'SkipLabelEqMilestone',pSkipLabelEqMilestone);
      Parts:=CheckPart(Parts,'CleanMileStones',pCleanMileStones);
      Parts:=CheckPart(Parts,'PriorityAsWeight',pPriorityAsWeight);
      Parts:=CheckPart(Parts,'SkipReproducibility',pSkipReproducibility);
      Parts:=CheckPart(Parts,'SkipDefaultLabels',pSkipDefaultLabels);
      Parts:=CheckPart(Parts,'Bugs',pBugs);
      Parts:=CheckPart(Parts,'Comments',pComments);
      Parts:=CheckPart(Parts,'Files',pFiles);
      Parts:=CheckPart(Parts,'Tags',pTags);
      Parts:=CheckPart(Parts,'CategoryLabels',pCategoryLabels);
      Parts:=CheckPart(Parts,'StandardLabels',pStandardLabels);
      Parts:=CheckPart(Parts,'PrivateComments',pPrivateComments);
      Parts:=CheckPart(Parts,'ExtraFieldLabels',pExtraFieldLabels);
      Parts:=CheckPart(Parts,'LinkIssues',pLinkIssues);
      Parts:=CheckPart(Parts,'LazLabelColors',pLazLabelColors);
      Parts:=CheckPart(Parts,'CrossProjectLinksOnly',pCrossProjectLinksOnly);
      Parts:=CheckPart(Parts,'TagsOnly',pTagsOnly);
      end;
  finally
    aIni.Free;
  end;
end;

function TMantis2GitlabApplication.ParseOptions : String;

Var
  ConfigFileName : string;
  I : Integer;

begin
  Result:='';
  FTimeStamp:=HasOption('t','timestamp');
  FQuiet:=HasOption('q','quiet');
  if HasOption('l','list-projects') then
    FRunMode:=rmListProjects
  else if HasOption('m','list-mantis-projects') then
    FRunMode:=rmListMantisProjects
  else if HasOption('s','list-milestones') then
    FRunMode:=rmListProjects
  else if HasOption('i','info') then
    FRunMode:=rmInfo
  else if HasOption('d','delete') then
    FRunMode:=rmDelete
  else
    FRunMode:=rmConvert;
  ConfigFileName:=GetOptionValue('c','config');
  if (ConfigFileName='') then
    ConfigFileName:=GetAppConfigFile(True);
  if Not HasOption('h','help') then
    begin
    if (ConfigFileName='') or not FileExists(ConfigFileName) then
      Result:='Config file does not exist: '+ConfigFileName
    else
      LoadConfig(ConfigFileName);
    end;
  I:=1;
  Repeat
    LogFileName:=ChangeFileExt(ConfigFileName,'-'+IntToStr(I)+'.log');
    Inc(I);
  until not FileExists(LogFileName);

end;

procedure TMantis2GitlabApplication.DoConversion;

Var
  aConversion : TMantisGitlabConverter;

begin
  aConversion:=TMantisGitlabConverter.Create(FConversionConfig,FGitlabClient,FMantisClient);
  try
    aConversion.OnLog:=@DoLog;

    aConversion.Execute;
  finally
    aConversion.Free;
  end;
end;

procedure TMantis2GitlabApplication.DoDelete;

  procedure DeleteResources(aName : String; IDField : String = ''; Params : Array of string);
  Var
    Resources : TJSONArray;
    aLen,aTotalCount,i,aCount,aPage : Integer;
    aID : Int64;
    baseURL : String;
    tParams : Array of string;

  begin
    tParams:=[];
    if IDField='' then
      idfield:='id';
    aTotalCount:=0;
    aPage:=1;
    BaseURL:=GetProjectResourceURL(aname);
    Repeat
      DoLog(Nil,Format('Fetching page %d of resources %s.',[aPage,aName]));
      setLength(tParams,Length(Params)+2);
      aLen:=Length(Params);
      For I:=0 to Length(Params)-1 do
        tParams[i]:=Params[I];
      tParams[aLen]:='per_page';
      tParams[aLen+1]:='100';
      //tParams[aLen+2]:='page';
      //tParams[aLen+3]:=IntToStr(aPage);
      Resources:=FGitlabClient.GetResourceList(BaseURL,tParams);
      try
        aCount:=Resources.Count;
        DoLog(Nil,Format('Got %d resources',[aCount]));
        For I:=0 to aCount-1 do
          begin
          // Writeln(Resources.Objects[I].aSJSON);
          aID:=Resources.Objects[I].Get(IDField,Int64(0));
          if aId<>0 then
            begin
            DoLog(Nil,'Deleting resource "'+aName+'", id : '+IntToStr(aID));
            FGitlabClient.DeleteResource(BaseURL+IntToStr(aID));
            Inc(aTotalCount);
            end;
          end;
      finally
        Resources.Free;
      end;
      inc(aPage);
    until (aCount<100);
    DoLog(Nil,Format('Deleted %d "%s" resources',[aTotalCount,aName]));
  end;

begin
  // DeleteResources('notes','iid');
  if pBugs in FConversionConfig.Parts then
    DeleteResources('issues','iid',['state','all']);
  if ([pVersionLabels,pCategoryLabels] * FConversionConfig.Parts)<>[] then
     DeleteResources('labels','',[]);
  if (pVersionMileStones in FConversionConfig.Parts) then
    DeleteResources('milestones','',[]);
end;

procedure TMantis2GitlabApplication.DoRun;
var
  ErrorMsg: String;
begin
  Terminate;

  ErrorMsg:=CheckOptions('hlc:midtq', ['help','list','config:','mantis-projects','info','delete','timestamp','quiet']);
  if (ErrorMsg='') and not HasOption('h','help') then
    ErrorMsg:=ParseOptions;
  if (ErrorMsg<>'') or HasOption('h','help') then
  begin
    Usage(ErrorMsg);
    Exit;
  end;
  FGitlabClient:=TGitlabClient.Create(FGitlabConfig);
  FGitlabClient.OnLog:=@DoLog;
  OpenLog;
  try
    Case RunMode of
      rmListProjects: DoListProjects;
      rmListMilestones: DoListMileStones;
      rmListMantisProjects : DoListMantisProjects;
      rmInfo : DoShowInfo;
      rmConvert : DoConversion;
      rmDelete : DoDelete;
    end;
  finally
    CloseLog;
  end;
end;

procedure TMantis2GitlabApplication.CloseLog;

begin
  FreeAndNil(Flog)
end;

procedure TMantis2GitlabApplication.OpenLog;

begin
  if LogFileName<>'' then
    FLog:=TFileStream.Create(LogFileName,fmCreate);
end;

procedure TMantis2GitlabApplication.ShowObject(aIndex: Integer;
  aObj: TJSONObject; aMembers: array of string);

  Function WriteMember(const aMember : string; D : TJSONData) : string;

  begin
    if Assigned(D) then
     if D.JSONType in [jtArray,jtObject] then
       Result:=aMember+': '+D.AsJSON
     else
       Result:=aMember+': '+D.AsString;
   end;

var
  I : Integer;
  aLine,aMember : string;
  D: TJSONData;

  Procedure AddToLine(aText : string);

  begin
    if aLine<>'' then
      aLine:=aLine+', ';
    aLine:=aLine+aText;
  end;

begin
  aLine:='';
  if Length(aMembers)=0 then
    begin
    for I:=0 to aObj.Count do
      AddToLine(WriteMember(aObj.Names[I],aObj.Items[i]));
    end
  else
     for aMember in aMembers do
       begin
       D:=aObj.Find(aMember);
       AddToLine(WriteMember(aMember,D));
       end;
  DoLog(Nil,Format('Item[%d]: %s',[aIndex,aLine]));
end;

procedure TMantis2GitlabApplication.ListObjects(aList: TJSONArray;
  aName: string; aMembers: array of string);

Var
  I,aCount : Integer;

begin
  DoLog(Nil,'Listing '+aName+':');
  if assigned(aList) then
    aCount:=aList.Count
  else
    aCount:=0;
  For I:=0 to aCount-1 do
    ShowObject(I,aList.Objects[i],aMembers);
  DoLog(Nil,IntToStr(aCount)+' objects.');
end;

procedure TMantis2GitlabApplication.DoListMantisProjects;

Var
  List : TJSONArray;

begin
  List:=FMantisClient.GetTableRecords('mantis_project_table',['id','name'],-1,1000);
  try
    ListObjects(List,'Projects', ['id','name']);
  finally
    List.Free;
  end;
end;


procedure TMantis2GitlabApplication.DoLog(Sender: TObject;
  const aMessage: string);

Var
  Msg : String;
begin
  Msg:='';
  if Assigned(Sender) then
   Msg:=Sender.ClassName+': ';
  Msg:=Msg+aMessage;
  if not FQuiet then
    Writeln(msg);
  if Assigned(Flog) then
     begin
     Msg:=Msg+sLineBreak;
     FLog.WriteBuffer(Msg[1],Length(Msg)*Sizeof(Char));
     end;

end;

procedure TMantis2GitlabApplication.DoListProjects;

Var
  List : TJSONArray;

begin
  List:=FGitlabClient.GetResourceList('projects/',['owned','1']);
  try
    ListObjects(List,'Projects', ['id','name']);
  finally
    List.Free;
  end;
end;

procedure TMantis2GitlabApplication.DoListMileStones;
Var
  List : TJSONArray;

  begin
    List:=FGitlabClient.GetResourceList(Format('projects/%d/milestones',[FConversionConfig.GitlabProjectID]),['per_page','100']);
    try
      ListObjects(List,'Milestones', []);
    finally
      List.Free;
    end;
end;

procedure TMantis2GitlabApplication.DoShowInfo;

Var
  P : TPart;

begin
  Writeln('Gitlab config:');
  With FGitlabConfig do
    begin
    Writeln('Base URL : ',BaseURL);
    Writeln('Token    : ',APIkey);
    end;
  Writeln;
  Writeln('Mantis config:');
  With FMantisClient.conn do
    begin
    Writeln('Database : ',DatabaseName);
    Writeln('Host     : ',HostName);
    Writeln('User     : ',UserName);
    Writeln('Password : ',Password);
    end;
  Writeln;
  Writeln('Conversion config:');
  With FConversionConfig do
    begin
    Writeln('Gitlab project ID             : ',GitlabProjectID);
    Writeln('Mantis project ID             : ',MantisProjectID);
    Writeln('Version label field           : ',TargetVersionLabelField);
    Writeln('Extra label field             : ',ExtraFieldName);
    Writeln('Extra label scope name/prefix : ',ExtraFieldLabel);
    Writeln('Extra label is unscoped       : ',ExtraFieldUnscoped);
    Writeln('Fixed in revision field       : ',FixedInRevisionField);
    Writeln('Bug files directory           : ',BugFileDir);
    Writeln('User map file                 : ',UserMapFile);
    Writeln('Bug ID to start from          : ',StartID);
    Writeln('Bug count                     : ',Count);
    Writeln('Project map file              : ',ProjectMapFile);
    Writeln('Mantis user map category      : ',UserMapCategory);
    Writeln('Mantis user map project ID    : ',UserMapProjectID);
    Writeln('Gitlab username=ID map file   : ',GitlabUserIDMapFile);
    Writeln('Exclude issue categories      : ',ExcludeCategories);
    Writeln('Only convert issue categories : ',OnlyCategories);
    for P in TPart do
      Writeln('Convert ',PadRight(Copy(GetEnumName(TypeInfo(P),Ord(P)),2),18),' : ',BoolToStr(P in Parts,'True','False'));
    end;
end;

constructor TMantis2GitlabApplication.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
  FMantisClient:=TMantisClient.Create();
  FMantisClient.OnLog:=@DoLog;
end;

destructor TMantis2GitlabApplication.Destroy;
begin
  FreeAndNil(FMantisClient);
  FreeAndNil(FGitlabClient);
  inherited Destroy;
end;

procedure TMantis2GitlabApplication.Usage(const aMsg: String);
begin
  { add your help code here }
  if (aMsg<>'') then
    Writeln('Error: '+aMsg);
  writeln('Usage: ', ExeName, ' [options]');
  writeln('where options is one or more of:');
  writeln('-h --help                  Help');
  writeln('-l --list-projects         List available projects on gitlab');
  writeln('-m --list-mantis-projects  List available projects on mantis');
  writeln('-s --list-milestones       List available milestones on gitlab');
  writeln('-c --config=file           Set the config file. Default: ',GetAppConfigFile(True));
  writeln('-d --delete                Delete all project resources');
  writeln('-t --timestamp             Print timestamp in front of messages');
  writeln('-q --quiet                 Do not print messages');
  ExitCode:=ord(aMsg='');
end;

var
  Application: TMantis2GitlabApplication;
begin
  Application:=TMantis2GitlabApplication.Create(nil);
  Application.Title:='Convert Mantis to Gitlab';
  Application.Run;
  Application.Free;
end.

