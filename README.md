# Mantis To Gitlab issue conversion

## Synopsis

Free Pascal project to completely convert a mantis project to a gitlab project.
Contrary to several projects found, this project attempts to do a complete 
conversion of bugs, categories, comments and attached files.

## To compile
You do not need the lazarus IDE to compile this project, but it will make life easier.
It should compile as-is with FPC. In the `src` directory, execute:
```sh
fpc -S2h mantistogitlab.pas
```


## Working. 
This is a command-line project, which is controlled by a configuration file in .ini format,
containing  all options that govern the conversion. a sample configuration file is included.

```sh
mantistogitlab -c yourconfig.cfg
```

## Conversion details 

The following labels are created in the gitlab project:

* Version (from extra field, set in versionlabelfield config setting)
* Category (from category table)

* Reproducibility (internal Mantis map) Can be skipped.
* Status (internal Mantis map)
* Resulution (internal Mantis map)
* Severity (internal Mantis map) 
* Priority (internal Mantis map) can be mapped to weight.

The following Milestones are created in the gitlab project:
* project_version table in mantis.

* fixed in version or target version can be set as labels or simply attached
  to milestone.

* When UpdateBugs is set in the configuration file, the status of the listed bug IDs are
  updated, notes and files are attached. UpdateBugs is @filename.txt (bug ids loaded from file)
  or a comma-separated list of IDS

   
